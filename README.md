---
# FractalTree
The branching extension to L-Systems allows them to simulate plant-

like structures, instead of simple fractals and algaes. It works by 

pushing onto a stack when a branch is started with “[”, and popping 

back when the branch is ended with “]”. 

---
Created a fractal tree using L-Systems:

![Scheme](bin/tree/1.PNG)
![Scheme](bin/tree/2.PNG)

--- 